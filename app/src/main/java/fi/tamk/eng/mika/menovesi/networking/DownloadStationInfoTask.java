package fi.tamk.eng.mika.menovesi.networking;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.URL;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import fi.tamk.eng.mika.menovesi.R;
import fi.tamk.eng.mika.menovesi.base.AppConstants;
import fi.tamk.eng.mika.menovesi.station.StationPrice;

/**
 * Created by mika on 05/12/2016.
 */

public class DownloadStationInfoTask extends AsyncTask<Void, Void, Set<StationPrice>> {

    /*
        Async delegates
    */
    public interface StationInfoResponseDelegate {
        void processFinish(ArrayList<StationPrice> prices);
    }

    private static final String TAG = DownloadStationInfoTask.class.getName();

    private final String PRICE_URL = AppConstants.API_BASE_URL + "/prices";

    private final Context context;
    private final StationInfoResponseDelegate delegate;

    public DownloadStationInfoTask(StationInfoResponseDelegate delegate, Context context) {
        this.context = context;
        this.delegate = delegate;
    }

    @Override
    protected Set<StationPrice> doInBackground(Void... voids) {

        Log.i(TAG, "Get preferred fuel type");
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

        String urlStr = PRICE_URL + "?fueltype=" + pref.getString(context.getString(R.string.key_pref_fuel_type), "");

        Log.i(TAG, "Fetching data from APi: " + urlStr);

        String json = "";

        try {
            URL url = new URL(urlStr);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            InputStream input = connection.getInputStream();

            if(input == null) {
                Log.e(TAG, "Null input stream");
                return null;
            }

            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line;

            String nl = AppConstants.NEWLINE;

            while ((line = reader.readLine()) != null) {
                buffer.append(line).append(nl);
            }

            if(buffer.length() == 0) {
                Log.e(TAG, "Buffer length is zero");
                return null;
            }

            json = buffer.toString();

        } catch (MalformedURLException me) {
            Log.e(TAG, "Malformed URL found");
            me.printStackTrace();
        } catch (IOException ioe) {
            Log.e(TAG, "Open connection failed");
            ioe.printStackTrace();
        } finally {
            return parseJson(json);
        }
    }


    private Set<StationPrice> parseJson(String json) {

        Set<StationPrice> prices = new HashSet<>();

        try {
            JSONArray array = new JSONArray(json);

            for(int i = 0; i < array.length() ; i++) {
                JSONObject object = array.getJSONObject(i);

                StationPrice price = new StationPrice(
                        object.getString("brand"),
                        object.getString("station"),
                        object.getString("type"),
                        object.getString("description"),
                        object.getDouble("pricePerUnit"),
                        new Timestamp(object.getLong("validFromTs")),
                        object.getDouble("lng"),
                        object.getDouble("lat"));

                prices.add(price);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            Log.i(TAG, "Found " + prices.size() + " prices");

            return prices;
        }
    }

    protected void onPostExecute(Set<StationPrice> prices) {
        Log.i(TAG, "onPostExecute");
        ArrayList<StationPrice> priceList = new ArrayList<>(prices);
        delegate.processFinish(priceList);
    }
}
