package fi.tamk.eng.mika.menovesi.stationlist;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;

import fi.tamk.eng.mika.menovesi.R;
import fi.tamk.eng.mika.menovesi.station.StationPrice;

/**
 * Created by mika on 01/11/2016.
 */

public class StationListArrayAdapter extends ArrayAdapter<StationPrice>{

    private Context context;
    private int layoutResourceId;
    List<StationPrice> data;

    private NumberFormat numberFormat;
    public StationListArrayAdapter(Context context, int resource, List<StationPrice> objects) {
        super(context, resource, objects);

        this.context = context;
        this.layoutResourceId = resource;
        this.data = objects;

        this.numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setMinimumFractionDigits(0);
        numberFormat.setRoundingMode(RoundingMode.HALF_UP);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        StationHolder holder = null;

        if(row == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new StationHolder();
            holder.stationName = (TextView)row.findViewById(R.id.station_name);
            holder.stationPrice = (TextView)row.findViewById(R.id.station_price);
            holder.stationDistance = (TextView)row.findViewById(R.id.station_distance);

            row.setTag(holder);
        } else {
            holder = (StationHolder)row.getTag();
        }

        StationPrice station = data.get(position);
        holder.stationName.setText(station.getStationName());
        holder.stationPrice.setText(station.getPricePerUnit() + " €/L");
        holder.stationDistance.setText(numberFormat.format(station.getDistance() / 1000) + " km");
        return row;
    }


    static class StationHolder {
        TextView stationName;
        TextView stationPrice;
        TextView stationDistance;
    }
}
