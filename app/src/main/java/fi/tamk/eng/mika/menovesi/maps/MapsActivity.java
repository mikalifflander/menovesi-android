package fi.tamk.eng.mika.menovesi.maps;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;
import java.util.ArrayList;

import fi.tamk.eng.mika.menovesi.R;
import fi.tamk.eng.mika.menovesi.base.AppConstants;
import fi.tamk.eng.mika.menovesi.base.GoogleApiHelper;
import fi.tamk.eng.mika.menovesi.base.MenuBarActivity;
import fi.tamk.eng.mika.menovesi.station.StationManager;
import fi.tamk.eng.mika.menovesi.station.StationPrice;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MapsActivity extends MenuBarActivity implements
        OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener {

    private static final String TAG = MapsActivity.class.toString();

    private static final int PERMISSION_REQUEST_FINE_LOCATION = 1;

    private static final float DEFAULT_ZOOM_LEVEL = 9.0f;

    private GoogleMap mMap = null;

    private String activeFuelType;
    private int activeRefreshTimer;

    private ArrayList<StationPrice> stationPrices;

    private DataReadyMessageReceiver messageReceiver;
    private IntentFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        requestLocationPermission();

        // Register for data ready broadcasts
        filter = new IntentFilter(AppConstants.INTENT_DATA_READY);
        messageReceiver = new DataReadyMessageReceiver();

        activeFuelType = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext())
                .getString(getString(R.string.key_pref_fuel_type), "");

        if(activeFuelType.equals("")) {
            activeFuelType = getString(R.string.default_pref_fuel_type);
            SharedPreferences.Editor editor = PreferenceManager
                    .getDefaultSharedPreferences(getApplicationContext())
                    .edit();

            editor.putString(getString(R.string.key_pref_fuel_type), activeFuelType);
            editor.apply();
        }

        loadIfNetworkAvailable(false);

        GoogleApiHelper.getInstance().requestLastLocation(getApplicationContext());
    }

    @Override
    protected void onResume() {
        registerReceiver(messageReceiver, filter);

        String _ft = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString(getString(R.string.key_pref_fuel_type), "");

        int _rt = Integer.parseInt(PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString(getString(R.string.key_pref_refresh_timer), ""));

        Log.i(TAG, "Active: " + activeFuelType + ", new: " + _ft);
        if (!activeFuelType.equals(_ft) || _rt != activeRefreshTimer) {
            activeFuelType = _ft;
            activeRefreshTimer = _rt;
            loadIfNetworkAvailable(true);
        }

        super.onResume();

    }

    @Override
    protected void onStart() {

        registerReceiver(messageReceiver, filter);
        super.onStart();
    }

    @Override
    protected void onStop() {

        unregisterReceiver(messageReceiver);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        StationManager.getInstance().cancelDataReload();
        super.onDestroy();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        marker.hideInfoWindow();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng home = new LatLng(61.654288, 24.360586600000033);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(home, DEFAULT_ZOOM_LEVEL));

        try {
            mMap.setMyLocationEnabled(true);
        } catch (SecurityException se) {
            Log.e(TAG, "User has not granted access to location");
        } finally {
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.setOnInfoWindowClickListener(this);
        }
    }

    private void requestLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // We need to request permission to use location
            if (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED) {
                Log.d(TAG, "Request Permission");

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_FINE_LOCATION);
            } else {
                Log.d(TAG, "Permission is already granted");
            }
        }
    }

    private boolean loadIfNetworkAvailable(boolean forceRefresh) {

        Log.i(TAG, "Verifying network connection");

        ConnectivityManager cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isConnected()) {

            String tStr = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.key_pref_refresh_timer),"");
            int timer = 0;
            try {
                timer = Integer.parseInt(tStr);
            } catch (NumberFormatException nfe) {
                Log.e(TAG, "Incorrect setting for refresh timer, reset defaults");

                try {
                    timer = Integer.parseInt(getString(R.string.default_pref_refresh_timer));
                    SharedPreferences.Editor editor = PreferenceManager
                            .getDefaultSharedPreferences(this)
                            .edit();

                    editor.putString(getString(R.string.key_pref_refresh_timer), timer+"");
                    editor.apply();


                }catch (NumberFormatException nfe2) {
                    Log.e(TAG, "Even default value failed. You should fix the software");
                    nfe2.printStackTrace();
                }
            } finally {
                activeRefreshTimer = timer;
            }
            Log.i(TAG, "Setting station manager to reload data every " + timer + " minutes");

            StationManager.getInstance().reloadData(this, timer, forceRefresh);

        } else {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_alert, (ViewGroup) findViewById(R.id.custom_toast_container));

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        }
        return true;
    }

    private void updateMapMarkers() {

        mMap.clear();

        double pricePerUnit = 0.0;

        for(StationPrice sp : this.stationPrices) {

            if(pricePerUnit == 0 )
                pricePerUnit = sp.getPricePerUnit();

            float color;
            if(pricePerUnit == sp.getPricePerUnit())
                color = BitmapDescriptorFactory.HUE_GREEN;
            else
                color = BitmapDescriptorFactory.HUE_RED;

            mMap.addMarker(new MarkerOptions()
                .position(sp.latLng())
                .title(sp.getStationName())
                .icon(BitmapDescriptorFactory.defaultMarker(color))
                .flat(true)
                .snippet(sp.getType() + ": " + sp.getPricePerUnit() + " €/L"));

        }
    }


    protected int getMenuId() {
        return R.id.mapViewMenuItem;
    }

    /*
    * Private class to handle notifications
    * */
    private class DataReadyMessageReceiver extends BroadcastReceiver {

        private final String TAG = DataReadyMessageReceiver.class.getName();

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceive: " + intent.getAction());

            switch (intent.getAction()) {

                case AppConstants.INTENT_DATA_READY:
                    stationPrices = StationManager.getInstance().getPriceList();

                    if(mMap != null)
                        updateMapMarkers();

                    break;

                default:
                    Log.w(TAG, "Unsupported intent received");
                    break;
            }

        }
    }
}
