package fi.tamk.eng.mika.menovesi.station;

import java.util.Comparator;

/**
 * Created by mika on 05/12/2016.
 */

public class StationPriceComparator implements Comparator<StationPrice> {

    @Override
    public int compare(StationPrice sp1, StationPrice sp2) {
        if(sp1.getPricePerUnit() < sp2.getPricePerUnit())
            return -1;
        if(sp2.getPricePerUnit() < sp1.getPricePerUnit())
            return 1;
        return 0;
    }
}
