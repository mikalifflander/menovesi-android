package fi.tamk.eng.mika.menovesi.station;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by mika on 27/10/2016.
 */

public class Station implements Parcelable {

    private String brand;

    private String name;

    public Station() {}

    public Station(String brand, String name) {
        this.brand = brand;
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.brand);
        dest.writeString(this.name);
    }

    protected Station(Parcel in) {
        this.brand = in.readString();
        this.name = in.readString();
    }

}
