package fi.tamk.eng.mika.menovesi.station;

import android.location.Location;
import android.os.Parcel;

import com.google.android.gms.maps.model.LatLng;

import java.sql.Timestamp;

/**
 * Created by mika on 27/10/2016.
 */

public class StationPrice extends Station {

    private String type;

    private String description;

    private Double pricePerUnit;
    private Double longitude, latitude;

    private float distance;

    private Timestamp validFrom;

    public StationPrice() {
        super();
    }

    public StationPrice(String brand, String station, String type, String description, double pricePerUnit, Timestamp validFrom, double lng, double lat) {
        super(brand, station);

        this.type = type;
        this.description = description;
        this.pricePerUnit = pricePerUnit;
        this.validFrom = validFrom;
        this.longitude = lng;
        this.latitude = lat;
    }

    public void setStation(String station) {
        super.setName(station);
    }
    public String getStation() {
        return super.getName();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Timestamp getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Timestamp validFrom) {
        this.validFrom = validFrom;
    }

    public String getStationName() {
        return this.getBrand() + " " + this.getName();
    }

    public LatLng latLng() { return  new LatLng(this.latitude, this.longitude); }

    @Override
    public int describeContents() {
        return 0;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setDistance(Location from){
        this.distance = from.distanceTo(this.location());
    }

    public float getDistance() {return this.distance;}

    public Location location() {
        Location loc = new Location("");
        loc.setLongitude(this.getLongitude());
        loc.setLatitude(this.getLatitude());
        return loc;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.type);
        dest.writeString(this.description);
        dest.writeValue(this.pricePerUnit);

        dest.writeSerializable(this.validFrom);
        dest.writeDouble(this.longitude);
        dest.writeDouble(this.latitude);
    }

    protected StationPrice(Parcel in) {
        super(in);
        this.type = in.readString();
        this.description = in.readString();
        this.pricePerUnit = (Double) in.readValue(Double.class.getClassLoader());
        this.validFrom = (Timestamp) in.readSerializable();
        this.longitude = (Double) in.readValue(Double.class.getClassLoader());
        this.latitude = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<StationPrice> CREATOR = new Creator<StationPrice>() {
        @Override
        public StationPrice createFromParcel(Parcel source) {
            return new StationPrice(source);
        }

        @Override
        public StationPrice[] newArray(int size) {
            return new StationPrice[size];
        }
    };
}
