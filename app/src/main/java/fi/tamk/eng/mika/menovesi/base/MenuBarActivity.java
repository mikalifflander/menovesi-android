package fi.tamk.eng.mika.menovesi.base;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.io.Serializable;

import fi.tamk.eng.mika.menovesi.R;
import fi.tamk.eng.mika.menovesi.maps.MapsActivity;
import fi.tamk.eng.mika.menovesi.settings.SettingsActivity;
import fi.tamk.eng.mika.menovesi.stationlist.StationListActivity;

/**
 * Created by mika on 01/11/2016.
 */

public abstract class MenuBarActivity extends AppCompatActivity {

    private static final String TAG = MenuBarActivity.class.getName();

    public MenuBarActivity() {}

    protected abstract int getMenuId();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.listViewMenuItem:
                intent = new Intent(getBaseContext(), StationListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);


                startActivity(intent);
                return true;
            case R.id.mapViewMenuItem:
                Log.i(TAG, "Map view clicked");
                intent = new Intent(getBaseContext(), MapsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                startActivity(intent);
                return true;
            case R.id.settingsViewMenuItem:
                Log.i(TAG, "Settings clicked");
                intent = new Intent(getBaseContext(), SettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

                startActivity(intent);
                return true;
            default:
                Log.e(TAG, "I should not be here");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        MenuItem item = menu.findItem(this.getMenuId());
        if(item != null)
            item.setVisible(false);


        return true;
    }
}
