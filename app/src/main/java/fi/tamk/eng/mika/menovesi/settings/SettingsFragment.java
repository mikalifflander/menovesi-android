package fi.tamk.eng.mika.menovesi.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import fi.tamk.eng.mika.menovesi.R;

/**
 * Created by mika on 05/12/2016.
 */

public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref_settings);
    }
}
