package fi.tamk.eng.mika.menovesi.station;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import fi.tamk.eng.mika.menovesi.R;

public class StationDetailActivity extends AppCompatActivity {

    private static final String TAG = StationDetailActivity.class.toString();

    private StationPrice station;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_detail);

        try {
            this.station = getIntent().getParcelableExtra("station");
            TextView brandTextView = (TextView) findViewById(R.id.brand_name);
            TextView stationTextView = (TextView) findViewById(R.id.station_name);
            TextView typeTextView  = (TextView) findViewById(R.id.type);

            brandTextView.setText(station.getBrand());
            stationTextView.setText(station.getStation());
            typeTextView.setText(station.getType());
        }catch (NullPointerException npe) {
            Log.e(TAG, "No intent or intent extra found");
        }finally {
            this.station.setBrand("");
        }

    }

    public void onSave(View view) {
        Log.i(TAG, "entry: onSave");

        Intent intent = new Intent(getBaseContext(), StationDetailActivity.class);
        intent.putExtra("station", this.station);

        setResult(RESULT_OK);
        finish();
    }

    protected int getViewId() {
        return R.layout.activity_station_list;
    }
}
