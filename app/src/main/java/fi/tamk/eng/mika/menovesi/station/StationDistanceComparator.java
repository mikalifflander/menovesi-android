package fi.tamk.eng.mika.menovesi.station;

import java.util.Comparator;

/**
 * Created by mika on 05/12/2016.
 */

public class StationDistanceComparator implements Comparator<StationPrice> {

    @Override
    public int compare(StationPrice sp1, StationPrice sp2) {
        if(sp1.getDistance() < sp2.getDistance())
            return -1;
        if(sp2.getDistance() < sp1.getDistance())
            return 1;

        return 0;
    }
}
