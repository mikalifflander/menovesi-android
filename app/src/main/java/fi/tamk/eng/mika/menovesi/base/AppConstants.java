package fi.tamk.eng.mika.menovesi.base;

/**
 * Created by mika on 05/12/2016.
 */

public class AppConstants {
    /*
    * OS Dependants
    * */
    public static final String NEWLINE = System.getProperty("line.separator");

    /*
    * API definitions
    * */
    public static final String API_BASE_URL = "http://home.tamk.fi/~c6mliffl/wap/practical/api/index.php";

    /*
    * Broadcast intents
    * */
    public static final String INTENT_DATA_READY = "dataReady";
    public static final String INTENT_LOCATION_UPDATE = "locationUpdate";

}
