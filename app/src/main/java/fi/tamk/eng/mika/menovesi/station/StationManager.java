package fi.tamk.eng.mika.menovesi.station;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import fi.tamk.eng.mika.menovesi.R;
import fi.tamk.eng.mika.menovesi.base.AppConstants;
import fi.tamk.eng.mika.menovesi.networking.DownloadStationInfoTask;

/**
 * Created by mika on 05/12/2016.
 *
 * Singleton
 * Handles all Station List related actions
 */
public class StationManager implements DownloadStationInfoTask.StationInfoResponseDelegate {

    // Debugging
    private static final String TAG = StationManager.class.getName();

    // Singleton
    private static StationManager ourInstance = new StationManager();
    public static StationManager getInstance() {
        return ourInstance;
    }

    // Variables
    private ArrayList<StationPrice> stationList;
    private Location currentLocation = null;

    private Context context;
    private ScheduledExecutorService scheduler = null;


    private StationManager() {
    }

    public void setStationList(ArrayList<StationPrice> stationList) {
        this.stationList = stationList;
    }

    public ArrayList<StationPrice> getStationList() {
        if(this.stationList == null)
            this.stationList = new ArrayList<>();

        return this.stationList;
    }

    /**
     * Get station list ordered by price per unit
     * if list is null, creates new list
     *
     * @return ordered list of stations
     */
    public ArrayList<StationPrice> getPriceList() {
        if(this.stationList == null)
            this.stationList = new ArrayList<>();

        if(!this.stationList.isEmpty())
            Collections.sort(stationList, new StationPriceComparator());
        return stationList;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;

        for(StationPrice sp : getStationList()) {
            sp.setDistance(currentLocation);
        }
    }

    /**
     * Get list of stations ordered by distance
     * If list is null, creates new empty list
     *
     * @return ordered list of stations
     */
    public ArrayList<StationPrice> getDistanceList() {
        if(this.stationList == null)
            this.stationList = new ArrayList<>();

        if(!this.stationList.isEmpty())
            Collections.sort(stationList, new StationPriceComparator());

        if(currentLocation == null)
            return this.stationList;

        for(StationPrice sp : this.stationList) {
            sp.setDistance(currentLocation);
        }

        Collections.sort(stationList, new StationDistanceComparator());
        return this.stationList;
    }

    /**
     * Uses background task to reload station data
     *
     * @param context Context for message broadcasts
     * @param refreshTimer Timer how often data should be refreshed in minutes, 0 to disable
     */
    public void reloadData(final Context context, int refreshTimer, boolean force) {

        if(scheduler != null && !scheduler.isShutdown() && !force)
            return;

        if(scheduler != null) {
            Log.i(TAG, "Force Refresh");

            scheduler.shutdownNow();
            scheduler = null;
        }

        this.context = context;

        if(refreshTimer > 0) {
            scheduler = Executors.newSingleThreadScheduledExecutor();
            scheduler.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    executeDataReload();
                }
            }, 0, refreshTimer, TimeUnit.MINUTES);
        } else {
            executeDataReload();
        }
    }

    public void cancelDataReload() {
        scheduler.shutdownNow();
        scheduler = null;
    }

    private void executeDataReload() {
        new DownloadStationInfoTask(this, this.context).execute();
        Toast.makeText(this.context, R.string.downloading_notification, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void processFinish(ArrayList<StationPrice> prices) {
        Log.i(TAG, "Data download is finished, time to notify UI");
        this.stationList = prices;

        Intent intent = new Intent(AppConstants.INTENT_DATA_READY);
        this.context.sendBroadcast(intent);
    }
}
