package fi.tamk.eng.mika.menovesi.stationlist;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import fi.tamk.eng.mika.menovesi.R;
import fi.tamk.eng.mika.menovesi.base.AppConstants;
import fi.tamk.eng.mika.menovesi.base.GoogleApiHelper;
import fi.tamk.eng.mika.menovesi.base.MenuBarActivity;

import fi.tamk.eng.mika.menovesi.station.StationManager;
import fi.tamk.eng.mika.menovesi.station.StationPrice;

public class StationListActivity extends MenuBarActivity {

    private static final String TAG = StationListActivity.class.toString();

    private ArrayList<StationPrice> stationList;

    private DataReadyMessageReceiver messageReceiver;
    private IntentFilter filter;

    private String activeFuelType;
    private int activeRefreshTimer;

    private StationListArrayAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_station_list);

        this.stationList = StationManager.getInstance().getDistanceList();

        mAdapter = new StationListArrayAdapter(this, R.layout.station_list_rel_row_item, stationList);

        ListView listView = (ListView) findViewById(R.id.station_list);

        View header = (View) getLayoutInflater().inflate(R.layout.station_list_header_row, null);

        filter = new IntentFilter(AppConstants.INTENT_DATA_READY);
        messageReceiver = new DataReadyMessageReceiver();

        listView.setAdapter(mAdapter);

        this.activeFuelType = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext())
                .getString(getString(R.string.key_pref_fuel_type), "");


        loadIfNetworkAvailable(false);

    }

    @Override
    protected void onStart() {

        registerReceiver(messageReceiver, filter);

        requestLocationUpdates();
        super.onStart();
    }

    @Override
    protected void onStop() {

        GoogleApiHelper.getInstance().stopLocationUpdates();

        unregisterReceiver(messageReceiver);
        super.onStop();
    }

    @Override
    protected void onResume() {
        registerReceiver(messageReceiver, filter);

        String _ft = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString(getString(R.string.key_pref_fuel_type), "");

        int _rt = Integer.parseInt(PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString(getString(R.string.key_pref_refresh_timer), ""));

        Log.i(TAG, "Active: " + activeFuelType + ", new: " + _ft);

        if (!activeFuelType.equals(_ft) || _rt != activeRefreshTimer) {
            activeFuelType = _ft;
            activeRefreshTimer = _rt;
            loadIfNetworkAvailable(true);
        }

        super.onResume();
    }

    @Override
    protected int getMenuId() {
        return R.id.listViewMenuItem;
    }

    private void requestLocationUpdates() {
        Log.i(TAG, "Requesting location updates");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        GoogleApiHelper.getInstance().requestLocationUpdates(getApplicationContext());
    }

    private boolean loadIfNetworkAvailable(boolean forceRefresh) {

        Log.i(TAG, "Verifying network connection");

        ConnectivityManager cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cManager.getActiveNetworkInfo();

        if(networkInfo != null && networkInfo.isConnected()) {

            String tStr = PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.key_pref_refresh_timer),"");
            int timer = 0;
            try {
                timer = Integer.parseInt(tStr);
            } catch (NumberFormatException nfe) {
                Log.e(TAG, "Incorrect setting for refresh timer, reset defaults");

                try {
                    timer = Integer.parseInt(getString(R.string.default_pref_refresh_timer));
                    SharedPreferences.Editor editor = PreferenceManager
                            .getDefaultSharedPreferences(this)
                            .edit();

                    editor.putString(getString(R.string.key_pref_refresh_timer), timer+"");
                    editor.apply();


                }catch (NumberFormatException nfe2) {
                    Log.e(TAG, "Even default value failed. You should fix the software");
                    nfe2.printStackTrace();
                }
            } finally {
                activeRefreshTimer = timer;
            }
            Log.i(TAG, "Setting station manager to reload data every " + timer + " minutes");

            StationManager.getInstance().reloadData(this, timer, forceRefresh);

        } else {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_alert, (ViewGroup) findViewById(R.id.custom_toast_container));

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        }
        return true;
    }

    /*
    * Message receiver
    * */
    private class DataReadyMessageReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case AppConstants.INTENT_DATA_READY:
                    Log.i(TAG, "Updating station list");
                    stationList = StationManager.getInstance().getDistanceList();
                    break;
                case AppConstants.INTENT_LOCATION_UPDATE:
                    Log.i(TAG, "Got location update intent");
                    break;
                default:
                    Log.w(TAG, "Unsupported intent received");
                    break;
            }

            mAdapter.data.clear();
            mAdapter.data.addAll(stationList);
            mAdapter.notifyDataSetChanged();
        }
    }
}
