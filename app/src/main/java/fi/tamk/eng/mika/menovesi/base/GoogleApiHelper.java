package fi.tamk.eng.mika.menovesi.base;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import fi.tamk.eng.mika.menovesi.R;
import fi.tamk.eng.mika.menovesi.maps.MapsActivity;
import fi.tamk.eng.mika.menovesi.station.Station;
import fi.tamk.eng.mika.menovesi.station.StationManager;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by mika on 06/12/2016.
 */
public class GoogleApiHelper implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener {
    private static final String TAG = GoogleApiHelper.class.getName();

    private static final int PERMISSION_REQUEST_FINE_LOCATION = 1;

    private static GoogleApiHelper ourInstance = new GoogleApiHelper();

    public static GoogleApiHelper getInstance() {
        return ourInstance;
    }

    private GoogleApiClient mGoogleApiClient;

    private Context context;
    private boolean locationUpdatesRequested = false;
    private boolean lastLocationRequested = false;

    private GoogleApiHelper() {
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Connected API Client");
        if(locationUpdatesRequested) {
            startLocationUpdates();
            locationUpdatesRequested = false;
        }
        if(lastLocationRequested) {
            try {
                StationManager.getInstance().setCurrentLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
            } catch (SecurityException se) {
                Toast.makeText(this.context, R.string.location_services_rejected, Toast.LENGTH_LONG).show();
            } finally {
                lastLocationRequested = false;
            }
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

       if (location == null)
            return;

        StationManager.getInstance().setCurrentLocation(location);
        Intent intent = new Intent(AppConstants.INTENT_LOCATION_UPDATE);
        context.sendBroadcast(intent);
    }

    public GoogleApiClient getGoogleApiClient(Context context) {
        if(mGoogleApiClient == null)
            buildAPIClient(context);

        if(!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();

        return mGoogleApiClient;
    }

    public void requestLastLocation(Context context) {
        if(this.getGoogleApiClient(context).isConnected()) {
            try {
                StationManager.getInstance().setCurrentLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
            } catch (SecurityException se) {
                Toast.makeText(this.context, R.string.location_services_rejected, Toast.LENGTH_LONG).show();
            }
        } else
            this.lastLocationRequested = true;
    }

    public void requestLocationUpdates(Context context) {

        if(this.getGoogleApiClient(context).isConnected())
            startLocationUpdates();
        else
            this.locationUpdatesRequested = true;
    }

    public void stopLocationUpdates() {
        Log.i(TAG, "Stopping location updates");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private void startLocationUpdates(){
        String distStr=PreferenceManager.getDefaultSharedPreferences(this.context).getString(context.getString(R.string.key_pref_distance_update),"");
        float distance=0;

        try{
            distance=Float.parseFloat(distStr);
        } catch(NumberFormatException nfe){
            distance = Float.parseFloat(context.getString(R.string.default_pref_distance_update));
            SharedPreferences.Editor editor=PreferenceManager.getDefaultSharedPreferences(context).edit();

            editor.putString(context.getString(R.string.key_pref_distance_update), context.getString(R.string.default_pref_distance_update));
            editor.apply();
        } finally{

            LocationRequest locationRequest=LocationRequest.create();
            locationRequest.setSmallestDisplacement(distance);
            locationRequest.setInterval(10000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            try {
                Log.i(TAG, "Started location updates for " + locationRequest.getSmallestDisplacement() + " meters");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
            } catch (SecurityException se) {
                Toast.makeText(this.context, R.string.location_services_rejected, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void buildAPIClient(Context context) {
        Log.i(TAG, "Entering: buildGoogleApiClient");

        this.context = context;
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

}
